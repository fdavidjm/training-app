class AddColumnProductIdInColours < ActiveRecord::Migration[5.2]
  def change
  		add_column :colours, :colour_id, :integer
  end
end
