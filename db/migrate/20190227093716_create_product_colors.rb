class CreateProductColors < ActiveRecord::Migration[5.2]
  def change
    create_table :product_colors do |t|
      t.string :product_id
      t.string :colour_id

      t.timestamps
    end
  end
end
