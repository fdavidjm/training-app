class AddColumnProductColourIdInProduct < ActiveRecord::Migration[5.2]
  def change
  	add_column :products, :colour_id, :integer
  end
end
