json.extract! colour, :id, :name, :description, :created_at, :updated_at
json.url colour_url(colour, format: :json)
