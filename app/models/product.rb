class Product < ApplicationRecord
	belongs_to :variety

	has_many :product_colour
	#has_many :colours through: :product_colour
	
	validates :name , :presence => true

	#default_scope where('name=test')

	#scope :name where(name='test')
end
