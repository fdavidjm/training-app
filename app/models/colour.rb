class Colour < ApplicationRecord	
	has_many :product_colour
	has_many :products through: :product_colour	

end
