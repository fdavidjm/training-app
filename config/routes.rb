Rails.application.routes.draw do
  root 'products#index'

  devise_for :users,
	controllers: {
		sessions: 'portal/sessions',
		:registrations => "portal/registrations",
		:passwords => "portal/passwords"
	},
	:path_names => { 
		:sign_in => 'login', 
		:sign_out => 'logout'
	}

  resources :colours
  resources :suppliers
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  resources :products
  resources :varieties
end
